[![Docker Pulls](https://img.shields.io/docker/pulls/sredelin/zookeeper.svg)](https://hub.docker.com/r/sredelin/zookeeper/)
[![Docker Stars](https://img.shields.io/docker/stars/sredelin/zookeeper.svg)](https://hub.docker.com/r/sredelin/zookeeper/)

zookeeper using docker
============

This image creates a ZooKeeper instance in a hardening way using docker.

## Pre-Requisites
1. install docker for your environment
2. install docker-compose for your environment

## how to build
you can easily build and start the image as container by using docker-compose
```
docker-compose build
docker-compose up -d
```
now a container of zookeeper started