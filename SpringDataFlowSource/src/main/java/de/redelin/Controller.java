package de.redelin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * this controller is used to start a process by incoming message
 * 
 * @author sredelin
 *
 */
@RestController
@RequestMapping("/source")
public class Controller {

	final static Logger LOG = LoggerFactory.getLogger(Controller.class);

	@Autowired
	DataProducer producer;
	@Autowired
	AnotherDataProducer anotherProducer;

	@RequestMapping(method = RequestMethod.GET , value = "/forward")
	public String Message(@RequestParam(name = "message", required = true) String message) {
		/*
		 * produce 20 messages with unique combination of id and directionKey
		 * and send them to producer
		 */
		for (int i = 0; i < 10; i++) {
			producer.sendMessage(HolyMessage.builder().text(message).id(i).directionKey("A").build());
			anotherProducer.sendMessage(HolyMessage.builder().text(message).id(i).directionKey("B").build());
		}
		return "sent";
	}
}