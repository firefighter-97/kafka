package de.redelin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;

/**
 * this producer sents some data to kafka using configuration based on
 * AnotherSource.class which constant matched with configuration in
 * application.yml
 * 
 * @author sredelin
 *
 */
@EnableBinding(AnotherSource.class)
public class AnotherDataProducer {
	final static Logger LOG = LoggerFactory.getLogger(AnotherDataProducer.class);

	@Autowired
	AnotherSource anotherSource;

	public void sendMessage(HolyMessage message) {
		LOG.info("Log message: " + message.getText());
		anotherSource.output().send(MessageBuilder.withPayload(message).build());
	}
}
