package de.redelin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;

/**
 * this producer sents some data to kafka using configuration based on
 * Source.class which constant matched with configuration in application.yml
 * 
 * @author sredelin
 *
 */
@EnableBinding(Source.class)
public class DataProducer {
	final static Logger LOG = LoggerFactory.getLogger(DataProducer.class);

	@Autowired
	Source source;

	public void sendMessage(HolyMessage message) {
		LOG.info("Log message: " + message.getText());
		source.output().send(MessageBuilder.withPayload(message).build());
	}
}
