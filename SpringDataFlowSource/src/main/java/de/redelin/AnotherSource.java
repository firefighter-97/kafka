package de.redelin;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface AnotherSource {
	String OUTPUT = "anotheroutput";

	@Output(AnotherSource.OUTPUT)
	MessageChannel output();
}
