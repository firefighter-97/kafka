package de.redelin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.annotation.ServiceActivator;

/**
 * this consumer reacts on configuration based on AnotherSink.class (input
 * constant is used to be identified in application.yml)
 * 
 * @author sredelin
 *
 */
@EnableBinding(AnotherSink.class)
public class AnotherDataConsumer {
	final static Logger LOG = LoggerFactory.getLogger(AnotherDataConsumer.class);

	@ServiceActivator(inputChannel = AnotherSink.INPUT)
	public void receiveMessage(final HolyMessage message) {
		LOG.info(String.format("Message is: %s with %d for direction %s", message.getText(), message.getId(),
				message.getDirectionKey()));
	}
}
