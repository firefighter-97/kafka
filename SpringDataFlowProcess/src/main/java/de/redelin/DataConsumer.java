package de.redelin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.integration.annotation.ServiceActivator;

/**
 * this consumer reacts on configuration based on Sink.class (input
 * constant is used to be identified in application.yml)
 * 
 * @author sredelin
 *
 */
@EnableBinding(Sink.class)
public class DataConsumer {
	final static Logger LOG = LoggerFactory.getLogger(DataConsumer.class);

	@ServiceActivator(inputChannel = Sink.INPUT)
	public void receiveMessage(final HolyMessage message) {
		LOG.info(String.format("Message is: %s with %d for direction %s", message.getText(), message.getId(),
				message.getDirectionKey()));
		/*
		 * message with id 2 should not be processable, so we can see an error
		 * message in processing and how kafka and spring cloud data flow react.
		 * Exception-Handling is bad in this implementation, because normally as
		 * example an error-topic should be used.
		 */
		if (message.getId() == 2) {
			LOG.error("ID 2 not legal!");
			throw new IllegalArgumentException("My Exception");
		}
	}

	@ServiceActivator(inputChannel = Sink.INPUT)
	public void receiveMessage(final String string) {
		LOG.info("not supported String object: " + string);
	}
}
