package de.redelin;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface AnotherSink {

	String INPUT = "anotherinput";

	@Input(AnotherSink.INPUT)
	SubscribableChannel input();
}
